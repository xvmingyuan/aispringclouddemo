package com.xmy.feign.impl;

import com.xmy.entity.Order;
import com.xmy.entity.PageModel;
import com.xmy.feign.OrderFeign;
import org.springframework.stereotype.Component;

@Component
public class OrderFeignImpl implements OrderFeign {
    @Override
    public void save(Order order) {

    }

    @Override
    public PageModel<Order> findAllByUid(long uid, int page, int limit) {
        return pageError();
    }

    @Override
    public void deleteByMid(long mid) {

    }

    @Override
    public void deleteByUid(long uid) {

    }

    @Override
    public PageModel<Order> findAllByState(int state, int page, int limit) {
        return pageError();
    }

    @Override
    public void updateState(Order order) {

    }

    public static PageModel<Order> pageError() {
        PageModel<Order> pageModel = new PageModel<>();
        pageModel.count = -1;
        pageModel.data = null;
        pageModel.code = 500;
        pageModel.msg = "服务访问失败,请联系服务人员";
        return pageModel;
    }
}
