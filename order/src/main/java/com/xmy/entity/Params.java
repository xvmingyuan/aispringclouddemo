package com.xmy.entity;

import lombok.Data;

@Data
public class Params {
    String key;
    String value;
    Integer seconds;
}
