package com.xmy.common.feigin;

import com.xmy.entity.Params;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "redisserver")
public interface RedisFeign {
    @PostMapping("/redis/set")
    public String set(@RequestBody Params params);

    @PostMapping("/redis/exists")

    public Boolean exists(@RequestBody Params params);

    @PostMapping("/redis/get")
    public String get(@RequestBody Params params);
}
