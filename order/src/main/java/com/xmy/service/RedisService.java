package com.xmy.service;


import com.xmy.common.redis.RedisCache;
import com.xmy.entity.Order;
import com.xmy.entity.PageModel;
import com.xmy.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import util.SPageModel;

@Service
public class RedisService {

    @Autowired
    private OrderRepository orderRepository;

    @RedisCache(type = PageModel.class)
    public PageModel<Order> findAllByUid(long uid, int page, int limit) {
        PageModel<Order> pm = SPageModel.PageModel.pageModel();
        pm.count = orderRepository.countByUid(uid);
        pm.data = orderRepository.findAllByUid(uid, page > 0 ? (page - 1) * limit : 0, limit > 0 ? limit : 10);
        return pm;
    }
}
