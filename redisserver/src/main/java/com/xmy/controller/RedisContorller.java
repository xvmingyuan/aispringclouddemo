package com.xmy.controller;

import com.xmy.entity.Params;
import com.xmy.redisserver.JedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/redis")
public class RedisContorller {
    @Autowired
    private JedisService jedisService;

    @PostMapping("/set")
    public String set(@RequestBody Params params) {
        jedisService.set(params.getKey(), params.getValue(), params.getSeconds());
        return "success";
    }

    @PostMapping("/exists")
    public Boolean exists(@RequestBody Params params) {
        return jedisService.exists(params.getKey());
    }

    @PostMapping("/get")
    public String get(@RequestBody Params params) {
        Object o = jedisService.get(params.getKey());
        if (o != null) {
            String json = String.valueOf(o);
            return json;
        }
        return null;
    }

}
